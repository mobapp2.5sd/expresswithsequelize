const Sequelize = require('sequelize')
const sequelize = new Sequelize('node_demo_db', 'root', '', {
    dialect: 'mysql'
})
sequelize.authenticate().then(() => {
    console.log('Connection established successfully.');
}).catch(err => {
    console.error('Unable to connect to the database:', err);
}).finally(() => {
    sequelize.close();
});
module.exports = sequelize