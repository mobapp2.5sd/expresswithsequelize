const express = require("express")
const router = express.Router()
const models = require("../models")
router.get("/", async (req, res) => {
    const employees = await models.Employee.findAll({
        raw: true,
        include: [{ //this is use to join tables here we extract data from company by using include(join)
            model: models.Company,

            attributes: [ // attributes is use to select specific columns
                "name"
            ]
        }],
        attributes: ["id", ["name", "employee"], "designation"] // ["name", "employee"] means select name and show name as employee
    })
    res.send(employees)
})
router.post("/", async (req, res) => {
    const employee = await models.Employee.create({
        name: req.body.name
    })
    if (!employee) return res.status(400).send({
        message: {
            error: "Error Occured"
        }
    })
    res.status(200).send({
        data: {
            status: "200",
            message: "Record Created"
        }
    })
})
router.put("/:id", async (req, res) => {
    const employee = await models.Employee.update({ //update({Key:Value which you want to update},{where...})
        name: req.body.name
    }, {
        where: {
            id: req.params.id
        }
    })

    if (!employee) return res.status(400).send({
        message: {
            error: "Error Occured"
        }
    })
    res.status(200).send({
        status: "200",
        message: "Record Updated"
    })
})
router.delete("/:id", async (req, res) => {
    const employee = await models.Employee.destroy({
        where: {
            id: req.params.id
        }
    })
    if (!employee) return res.status(400).send({
        message: {
            error: "Error Occured"
        }
    })
    res.status(200).send({
        status: "200",
        message: "Record Deleted"
    })
})
module.exports = router