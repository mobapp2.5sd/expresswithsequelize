const express = require("express")
const router = express.Router()
const Sequelize = require('sequelize')
const models = require("../models")
router.get("/", async (req, res) => {
    const companies = await models.Company.findAll({
        raw: true,
        attributes: ["id", "name"]
    })
    res.send({
        data: {
            status: "200",
            companies: companies
        }
    })

})
router.post("/", async (req, res) => {
    if (!req.body.name) return res.status(400).send({
        message: {
            error: "Error Occured"
        }
    })
    const company = await models.Company.create({

        name: req.body.name
    })
    if (!company) return res.status(400).send("Error Occured")
    res.status(200).send({
        data: {
            status: "200",
            message: "Record Created"
        }
    })
})
router.put("/:id", async (req, res) => {

    const company = await models.Company.findOne({
        where: {
            id: req.params.id
        }
    })

    if (!company) return res.status(400).send("Error Occured")
    const isUpdate = await models.Company.update({
        name: req.body.name
    }, {
        where: {
            id: req.params.id
        }
    })
    if (!isUpdate) return res.status(400).send("Error Occured")
    res.status(200).send({
        data: {
            status: "200",
            message: "Record Updated"
        }
    })
})
router.delete("/:id", async (req, res) => {
    console.log(req.params.id)
    const isDel = await models.Company.destroy({
        where: {
            id: req.params.id
        }
    })
    if (!isDel) return res.status(400).send("Error Occured")
    res.status(200).send({
        data: {
            status: "200",
            message: "Record Deleted"
        }
    })
})

module.exports = router